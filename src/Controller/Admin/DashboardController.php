<?php

namespace App\Controller\Admin;

use App\Entity\Artiste;
use App\Entity\Concert;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration de la siréne');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Retour au site', 'fas fa-sign-out-alt', 'homepage');
        yield MenuItem::section('Admin', 'fas fa-cogs');
        yield MenuItem::linkToCrud('Les concerts', 'fas fa-volume-up', Concert::class);
        //yield MenuItem::linkToCrud('Les concerts passés', 'fas fa-volume-down', Concert::class);
        yield MenuItem::linkToCrud('Artiste', 'fas fa-user-cog', Artiste::class);

    }
}
